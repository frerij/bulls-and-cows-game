#this will execute a console game of bulls and cows
#   the computer will select a secret number (unique values), and the user
#   will have to guess to get the answer
#the computer will reveal whether numbers are in the exact
#   correct place (bulls) or in the number but in other positions (cows)

from random import randint


def make_secret_number():
    secret_number = []
    while len(secret_number) < 4:
        new_int = randint(0, 9)
        if (new_int not in secret_number):
            secret_number.append(new_int)
    return secret_number

def str_to_list(guess):
    guess_list = []
    for i in guess:
        num_guessed = int(i)
        guess_list.append(num_guessed)
    return guess_list

def exact_matches(guess_list, secret_number):
    bulls = 0
    for a,b in zip(secret_number, guess_list):
        if a == b:
            bulls += 1
    return bulls

def close_matches(guess_list, secret_number):
    cows = 0
    for i in guess_list:
        if i in secret_number:
            cows += 1
    return cows


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers")
    print("Can you guess it?")
    print("You can guess 20 times. \n")

    secret_number = make_secret_number()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        while len(guess) !=  4:
            prompt = "Guess is not 4 digits, please enter a 4 digit number"
            guess = input(prompt)
    
        #guess string to list function call
        converted_guess = str_to_list(guess)

        total_exact_matches = exact_matches(converted_guess, secret_number)

        total_close_matches = close_matches(converted_guess, secret_number)

        if total_exact_matches == 4:
            print("You guessed the right number!!")
            return
        
        print("total number of bulls: ", total_exact_matches)

        print("total number of cows: ", total_close_matches - total_exact_matches)

        
def run():
    response = input("Do you want to play a game? (Y/N) \n")

    while response == "Y":
        play_game()
        response = input("Do you want to play a game? \n")



if __name__ == "__main__":
    run()